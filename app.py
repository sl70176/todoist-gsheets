#!/usr/local/venv/todoist-gsheets/bin/python
from datetime import datetime
import gspread
import json
import todoist
import pytz


def main():
    with open("/srv/todoist-gsheets/secrets.json", "r") as f:
        secrets = json.load(f)
    gc = gspread.service_account(filename="/srv/todoist-gsheets/gcp-credentials.json")
    todoist_api = todoist.TodoistAPI(secrets["todoist_api_key"])
    sh = gc.open_by_key(secrets["sheet_id"]) 
    task_ids = [int(x.title) for x in sh.worksheets() if x.title.isdigit()]
    completed_items = todoist_api.completed.get_all()["items"]
    completed_items.reverse()
    utc = pytz.timezone("UTC")
    america_toronto = pytz.timezone("America/Toronto")
    for t in task_ids:
        worksheet = sh.worksheet(str(t))
        first_value = worksheet.get('A1').first()
        if first_value is None:
            first_value = datetime(1, 1, 1, 0, 0)
        else:
            first_value = datetime.strptime(first_value, "%Y-%m-%d %H:%M:%S")
        for i in completed_items:
            if i["task_id"] == t:
                utc_timestamp = utc.localize(datetime.strptime(i["completed_date"], "%Y-%m-%dT%H:%M:%SZ"))
                toronto_timestamp = utc_timestamp.astimezone(america_toronto)
                clean_date = toronto_timestamp.replace(tzinfo=None)
                if clean_date <= first_value:
                    continue
                else:
                    worksheet.insert_row([str(clean_date)], index=1, value_input_option='USER_ENTERED')
                    worksheet.update_acell("A1", str(clean_date).replace("'"," "))


if __name__ == "__main__":
    main()
