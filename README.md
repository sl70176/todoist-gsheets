# todoist-gsheets

Todoist / Google Sheets integration

## Prerequisites

In order to use this project you need the following files,
which should not be committed to the repo:

- gcp-credentials.json - the JSON file for a GCP service account
- secrets.json - see sample

You need to enable the Google Sheets API in your project, and
share the Google sheet with the email address of the GCP service account.

This may be helpful for creating the service account:
http://gspread.readthedocs.org/en/latest/oauth2.html
